# MODWM - Kreato's fork 

# Preview
![preview](preview.png)

## Added Stuff
1.bind rofi to modkey

2.add script for maim

also this is not the latest commit because the latest commits broken.btw i am using 
gruvbox-dark-soft theme on rofi to fit the theme

## Dependencies
Fonts: JetBrains Mono & Siji

maim

Rofi

## Installing
Run:
```c
make install
```
Then you can put something like this at the end of your ~/.xinitrc file:
```c
exec dwm
```

# Credits

HUGE Thanks to [jab2870](https://git.jonathanh.co.uk/jab2870/Dotfiles/src/branch/master/bin/.bin/screenshot) for the maim script.

Thanks to [plasmoduck](https://github.com/plasmoduck/modwm) for modwm.
